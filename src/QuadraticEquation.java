import java.util.Scanner;

public class QuadraticEquation {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a value of the number a: ");
        double a = in.nextDouble();
        System.out.print("Enter a value of the number b: ");
        double b = in.nextDouble();
        System.out.print("Enter a value of the number c: ");
        double c = in.nextDouble();

        double d = b * b - 4 * a * c;
        System.out.println("The value of the discriminant: D = " + d);

        if (d > 0) {
            double x1 = (-b + Math.sqrt(d)) / (2 * a);
            double x2 = (-b - Math.sqrt(d)) / (2 * a);
            System.out.println("The equation has two roots: x1 = " + x1 + ", x2 = " + x2);
        } else if (d == 0) {
            double x;
            x = -b / (2 * a);
            System.out.println("The equation has one root: x = " + x);
        } else {
            System.out.println("The equation has no roots");
        }
    }
}
